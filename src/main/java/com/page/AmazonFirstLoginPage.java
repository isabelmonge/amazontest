package com.page;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;



@DefaultUrl("https://www.amazon.es/ap/signin?openid.return_to=https%3A%2F%2Fwww.amazon.es%2F%3Fref_%3Dnav_ya_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=esflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&&openid.pape.max_auth_age=0")
public class AmazonFirstLoginPage extends PageObject {
    @CacheLookup
    @FindBy(id = "ap_email")
    private WebElement userInputField;
    @FindBy(id = "continue")
    private WebElement continueButton;

    public AmazonFirstLoginPage(WebDriver driver) {
        super(driver);
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
    }

    @WhenPageOpens
    public void waitUntilAmazonLogoAppears() {
        $("/html/body/div[1]/div[1]/div[1]/div/a/i[1]").waitUntilVisible();
    }

    public AmazonSecondLoginPage fillUser(String user) {
        element(userInputField).clear();
        element(userInputField).typeAndEnter(user);
        return new AmazonSecondLoginPage();
    }
}
