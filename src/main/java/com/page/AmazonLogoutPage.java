package com.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;

@DefaultUrl("https://wwww.amazon.es/")

public class AmazonLogoutPage extends PageObject {

    @CacheLookup
    @FindBy(id = "nav-link-accountList")
    private WebElement btnCuenta;


    public AmazonLogoutPage(WebDriver driver) {

        super(driver);
        driver.manage().window().maximize();

    }


    public void clickCuenta(String buttonCuenta) {

    }

    public void clickCerrarSesion(String buttonCerrarSesion) {

        getDriver().findElement(By.xpath("/html/body/div[1]/header/div/div[1]/div[4]/div[3]/div[2]/div/div[2]/a[16]/span")).click();
    }
}
