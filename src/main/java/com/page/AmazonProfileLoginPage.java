package com.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

public class AmazonProfileLoginPage extends PageObject {

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"nav-your-amazon-text\"]")
    private WebElement actualText;

    public AmazonProfileLoginPage(WebDriver driver) {
        super(driver);
    }

    @WhenPageOpens
    public void waitForPage() {
        // wait for amazon logo
        $("#//html/body/div[1]/header/div/div[1]/div[1]/div/a[1]/span[1]").waitUntilVisible();
    }


    public String getProfileText() {
        String text = element(actualText).getText();
        return text;
    }
}
