package com.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by operador on 14/12/2016.
 */
public class AmazonResultsPage extends PageObject {


    @CacheLookup
    @FindBy(xpath = "//h2")
    private List<WebElement> linkTitlesList;

    public AmazonResultsPage(WebDriver driver) {
        super(driver);
    }


    @WhenPageOpens
    public void waitForPage() {
        // wait for amazon logo
        $("#//html/body/div[1]/header/div/div[1]/div[1]/div/a[1]/span[1]").waitUntilVisible();
    }

    public List<String> getResultsList() {
        List<String> resultList = new ArrayList<>();
        for (WebElement element : linkTitlesList) {
            resultList.add(element.getText());
        }
        return resultList;
    }
}
