package com.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

/**
 * Created by operador on 14/12/2016.
 */
@DefaultUrl("https://amazon.es")
public class AmazonSearchPage extends PageObject {

    @CacheLookup
    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchInputField;

    public AmazonSearchPage(WebDriver driver) {
        super(driver);
        driver.manage().window().maximize();
    }

    @WhenPageOpens
    public void waitUntilAmazonLogoAppears() {
        $("//*[@id=\"nav-logo\"]/a[1]/span[1]").waitUntilVisible();
    }

    public AmazonResultsPage searchFor(String searchRequest) {
        element(searchInputField).clear();
        element(searchInputField).typeAndEnter(searchRequest);
        return new AmazonResultsPage(getDriver());
    }
}
