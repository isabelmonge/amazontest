package com.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.WhenPageOpens;
import org.junit.AfterClass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

public class AmazonSecondLoginPage extends PageObject {

    @CacheLookup
    @FindBy(id = "ap_password")
    private WebElement passwordInputField;

    @WhenPageOpens
    public void waitUntilAmazonLogoAppears() {
        $("/html/body/div[1]/div[1]/div[1]/div/a/i[1]").waitUntilVisible();
    }


    public AmazonProfileLoginPage fillPasword(String password) {
        element(passwordInputField).clear();
        element(passwordInputField).typeAndEnter(password);
        return new AmazonProfileLoginPage(getDriver());
    }

}
