package com.serenitySteps;

import com.page.AmazonFirstLoginPage;
import com.page.AmazonProfileLoginPage;
import com.page.AmazonSecondLoginPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class AmazonLoginSteps {

    AmazonFirstLoginPage firstLoginPage;
    AmazonSecondLoginPage secondLoginPage;
    AmazonProfileLoginPage profileLoginPage;


    @Step
    public void openAmazonFirstLoginPage() {

        firstLoginPage.open();
    }

    @Step
    public void fillUser(String user){

        firstLoginPage.fillUser(user);
    }


    @Step
    public void fillPassword(String password){
        secondLoginPage.fillPasword(password);
    }



    @Step
    public void verifyName(String name){
        String actualText =  profileLoginPage.getProfileText();
        Assert.assertTrue(actualText.contains("Amazon.es"));

    }
}
