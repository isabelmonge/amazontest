package com.serenitySteps;

import com.page.AmazonFirstLoginPage;
import com.page.AmazonLogoutPage;
import cucumber.api.java.Before;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AmazonLogoutSteps {

    AmazonLogoutPage logoutPage;

//    @Before("@loginUser")
//    public void beforeScenario(){
//
//    }

    @Step
    public void clickCuenta(String buttonCuenta) {
    }

    @Step
    public void clickCerrarSesion(String buttonCerrarSesion) {
        logoutPage.clickCerrarSesion(buttonCerrarSesion);
    }
}
