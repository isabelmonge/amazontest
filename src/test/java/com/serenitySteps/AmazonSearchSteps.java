package com.serenitySteps;

import com.page.AmazonResultsPage;
import com.page.AmazonSearchPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.List;

/**
 * Created by operador on 14/12/2016.
 */
public class AmazonSearchSteps {

    AmazonSearchPage searchPage;
    AmazonResultsPage resultsPage;

    @Step
    public void openAmazonSearchPage() {
        searchPage.open();
    }

    @Step
    public void searchFor(String searchRequest) {

        resultsPage = searchPage.searchFor(searchRequest);
    }

    @Step
    public void verifyResult(String searchResult) {
        List<String> results = resultsPage.getResultsList();
        Assert.assertTrue(results.contains(searchResult));
    }
}
