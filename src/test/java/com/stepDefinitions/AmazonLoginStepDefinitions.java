package com.stepDefinitions;



import com.serenitySteps.AmazonLoginSteps;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;



public class AmazonLoginStepDefinitions {

    @Steps
    AmazonLoginSteps amazonLoginSteps;

    @Given("^I am on Login Amazon Page$")
    public void iAmOnLoginAmazonPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        amazonLoginSteps.openAmazonFirstLoginPage();

    }

    //    @When("^I fill '^(.+)@(.+)$'$")

    @When("^I fill \"([^\"]*)\"$")
    public void iFillUser(String user) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        amazonLoginSteps.fillUser(user);
    }


    @And("^I fill on \"([^\"]*)\"$")
    public void iFillOn(String password) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        amazonLoginSteps.fillPassword(password);

    }


    @Then("^I should see name contains \"([^\"]*)\"$")
    public void iShouldSeeNameContains(String name) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        amazonLoginSteps.verifyName(name);

    }
}
