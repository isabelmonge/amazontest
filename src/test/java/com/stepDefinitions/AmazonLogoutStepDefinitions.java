package com.stepDefinitions;


import com.serenitySteps.AmazonLogoutSteps;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class AmazonLogoutStepDefinitions {

    @Steps
    AmazonLogoutSteps amazonLogoutSteps;

    @Before("loginUser")
    public void loginUser() {

    }

    @When("^I click on \"([^\"]*)\"$")
    public void iClickOn(String buttonCuenta) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        amazonLogoutSteps.clickCuenta(buttonCuenta);

    }

    @And("^I click \"([^\"]*)\"$")
    public void iClick(String buttonCerrarSesion) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        amazonLogoutSteps.clickCerrarSesion(buttonCerrarSesion);
    }

    @Then("^I logout my Amazon account$")
    public void iLogoutMyAmazonAccount() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }
}
