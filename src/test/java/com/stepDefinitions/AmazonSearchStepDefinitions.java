package com.stepDefinitions;

import com.serenitySteps.AmazonSearchSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Created by operador on 14/12/2016.
 */
public class AmazonSearchStepDefinitions {

    @Steps
    AmazonSearchSteps amazonSearchSteps;

    @Given("I want to search in Amazon")
    public void iWantToSearchInAmazon() throws Throwable {
        amazonSearchSteps.openAmazonSearchPage();
    }

    @When("I search for '(.*)'")
    public void iSearchFor(String searchRequest) throws Throwable {
        amazonSearchSteps.searchFor(searchRequest);
    }

    @Then("I should see link to '(.*)'")
    public void iShouldSeeLinkTo(String searchResult) throws Throwable {
        amazonSearchSteps.verifyResult(searchResult);
    }

}
