@loginTest
Feature: Amazon Login
  In order to login
  As a generic user
  I should see profile page

  @loginSimple
  Scenario: Amazon Login
    Given I am on Login Amazon Page
    When  I fill "imonge3941@gmail.com"
    And  I fill on "qwerty123456"
    Then I should see name contains "Isabel"


  Scenario Outline: Amazon Login
    Given I am on Login Amazon Page
    When  I fill "<user>"
    And  I fill on "<password>"
    Then I should see name contains "<name>"

    Examples:
      | user                  | password      | name   |
      | isabel.monge@alten.es | 123456qwerty  | Isabel |
      | prueba@yopmail.com    | aaaaaaaaaaaaa | Prueba |

