@logoutTest
Feature: Amazon Logout
  In order to logout
  As a generic user
  I should log out successfully


  Background:
    Given I am on Login Amazon Page
    When  I fill "imonge3941@gmail.com"
    And  I fill on "qwerty123456"
  Scenario:
    When I click on "Cuenta y listas"
    And I click "Cerrar sesión"
    Then I logout my Amazon account