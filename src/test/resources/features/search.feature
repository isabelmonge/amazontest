@searchTest
Feature: Amazon search
  In order to find items
  As a generic user
  I want to be able to search with Amazon

  Scenario: Amazon search
    Given I want to search in Amazon
    When I search for 'Falda'
    Then I should see link to 'FIND Falda Metálica Plisada para Mujer'


  Scenario Outline: Amazon search multiple
    Given I want to search in Amazon
    When I search for '<search_request>'
    Then I should see link to '<search_result>'
    Examples:
      | search_request | search_result                      |
      | kindle         | aaaaa                              |
      | Cucumber       | Cucumber Quest: The Melody Kingdom |
      | Cucumber       | Cucumber Quest                     |